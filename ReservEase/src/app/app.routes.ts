import { Routes } from '@angular/router';
import { SigninComponent } from './Pages/signin/signin.component';
import { HomeComponent } from './Pages/home/home.component';
import { SignupComponent } from './Pages/signup/signup.component';
import { NavComponent } from './Pages/nav/nav.component';
import { FootComponent } from './Pages/foot/foot.component';
import { MyaccountComponent } from './Pages/myaccount/myaccount.component';
import { ReservationsComponent } from './Pages/reservations/reservations.component';
import { ResourcesComponent } from './Pages/resources/resources.component';
import { SpacesComponent } from './Pages/spaces/spaces.component';

export const routes: Routes = [
    {path:'',component:HomeComponent},
    {path:'signin',component:SigninComponent},
    {path:'signup',component:SignupComponent},
    {path:'myaccount',component:MyaccountComponent},
    {path:'reservations',component:ReservationsComponent},
    {path:'resources',component:ResourcesComponent},
    {path:'spaces',component:SpacesComponent}
];
