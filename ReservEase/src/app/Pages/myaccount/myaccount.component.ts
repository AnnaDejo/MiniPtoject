import { Component } from '@angular/core';
import { RouterLink } from '@angular/router';


@Component({
  selector: 'app-myaccount',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './myaccount.component.html',
  styleUrl: './myaccount.component.css'
})
export class MyaccountComponent {
  inputValue: string = ''; // Initial value of the input
  editing: boolean = false; // Initial state of editing

  toggleEdit() {
    this.editing = !this.editing;
  }
}
