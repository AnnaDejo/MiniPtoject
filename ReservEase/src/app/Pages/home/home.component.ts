import { Component } from '@angular/core';
import { NavComponent } from '../nav/nav.component';
import { FootComponent } from "../foot/foot.component";
import { RouterLink } from '@angular/router';
import { SigninComponent } from '../signin/signin.component';
import { SignupComponent } from '../signup/signup.component';

@Component({
    selector: 'app-home',
    standalone: true,
    templateUrl: './home.component.html',
    styleUrl: './home.component.css',
    imports: [NavComponent, FootComponent,RouterLink]
})
export class HomeComponent 
{
  
}
