import { Component } from '@angular/core';
import { RouterLink } from '@angular/router';
import { NavComponent } from "../nav/nav.component";

@Component({
    selector: 'app-reservations',
    standalone: true,
    templateUrl: './reservations.component.html',
    styleUrl: './reservations.component.css',
    imports: [RouterLink, NavComponent]
})
export class ReservationsComponent {

}
