import { Component } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';
import { SignupComponent } from './Pages/signup/signup.component';
import { SigninComponent } from './Pages/signin/signin.component';
import { HomeComponent } from './Pages/home/home.component';
import { NavComponent } from './Pages/nav/nav.component';
import { FootComponent } from './Pages/foot/foot.component';
import { MyaccountComponent } from './Pages/myaccount/myaccount.component';
import { ReservationsComponent } from "./Pages/reservations/reservations.component";
import { ResourcesComponent } from './Pages/resources/resources.component';
import { SpacesComponent } from './Pages/spaces/spaces.component';

@Component({
    selector: 'app-root',
    standalone: true,
    templateUrl: './app.component.html',
    styleUrl: './app.component.css',
    imports: [
      RouterOutlet, SignupComponent, SigninComponent, HomeComponent, FootComponent, MyaccountComponent, RouterLink, NavComponent, ReservationsComponent,ResourcesComponent,SpacesComponent
    ]
})
export class AppComponent {
  title = 'ReservEase';
}
